# Bienvenue sur mon site!
## A destination des élèves
Vous pourrez y trouver le cours, les exercices mais aussi des vidéos en rapport avec le cours. Principalement à destination de mes élèves de 2^nde^ en SNT et en Mathématiques.
## A destination des enseignants
Si ce site vous a aidé ou si vous avez une question, nhésitez pas à me contacter!  
  
  Bonne visite:)