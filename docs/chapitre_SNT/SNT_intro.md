---
author: Sandra Da Silva 
title: SNT - Introduction 
---

# Sciences Numérique et Technologie

*Sandra Da Silva*

*Enseignante d'Informatique et de Mathématiques au lycée Fernand Darchicourt d'Hénin-Beaumont*

--------------------

## Consignes de rentrée

La cours s'articule entre travaux pratiques en demi-groupes à raison d'une heure par semaine et cours-activités en classe entière à raison d'une heure tous les quinze jours.

Evaluation:

- Evaluation écrite : sous forme de questions classiques et QCM. 
Chaque évaluation comporte des questions se rapportant à du travail qui était à faire à la maison.
- Evaluation orale : dans l'année les élèves auront à intervenir devant la classe sur des sujets divers.
- Evaluation pratique : certaines activités pratiques seront évaluées et inégrées à la moyenne.
- Evaluation de l'investissement et du sérieux : lors de toutes les activités, sous forme de bonus à la moyenne.

## Présentation

[Introduction SNT](a_telecharger/IntroductionSNT.pdf){ .md-button target="_blank" rel="noopener" }

## Informatique et numérique

!!! info "Ma vidéo"
    <div class="centre">
    <iframe 
    src="https://player.vimeo.com/video/122104210?autoplay%253D1" 
    width="640" height="360" 
    frameborder="0" 
    allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
    </iframe>
    </div>

!!! question "QCM"
    {{multi_qcm(
    ["Quelle est la réponse à la question universelle ? Cocher deux réponses.", 
    ["$6\\times 7$", "$\\int_0^{42} 1 dx$", "Je ne sais pas", " `#!py sum([i for i in range(10)])`", "La réponse D"], [1, 2]],
    ["1 + 1 = ? Cocher deux réponses.",
    ["Je ne sais pas", "2", "L'âge du capitaine", "10 en binaire"], [2, 4]]
    )}}
